<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravolt\Suitable\AutoSort;
use Laravolt\Support\Traits\AutoSearch;

class Artikel extends Model
{
    use AutoSearch, AutoSort;
    protected $table = 'artikel';
    protected $searchableColumns = ['judul','isi'];
    protected $fillable = ['judul','isi','user_id'];
    public function kategori(){
        return $this->belongsToMany(Kategori::class);
    }
}
