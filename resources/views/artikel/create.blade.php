@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Tambah Artikel"/>

    {!! form()->open(route('artikel.store')) !!}
    {!! form()->text('judul')->label('Judul') !!}
    {!! form()->textarea('isi')->label('isi') !!}
    {!! form()->dropdown('kategori', $kategori)->label('Kategori') !!}
    {!! form()->link('<i class="ui icon angle left"></i>Batal', route('artikel.index')) !!}
    {!! form()->submit('Kirim') !!}
    {!! form()->close() !!}

@endsection
