@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Artikel">
        <x-item>
            <x-link label="Tambah" icon="plus" url="{{route('artikel.create')}}"></x-link>
        </x-item>
    </x-titlebar>

    {!! Suitable::source($artikel)->search()->columns([
        \Laravolt\Suitable\Columns\Numbering::make('No'),
        \Laravolt\Suitable\Columns\Text::make('judul', 'Judul')->sortable('judul'),
       \Laravolt\Suitable\Columns\Text::make('isi', 'Isi')])->render()
    !!}

@endsection
