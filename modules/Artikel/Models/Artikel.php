<?php

namespace Modules\Artikel\Models;

use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;

class Artikel extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'artikel';

    protected $guarded = [];

    protected $searchableColumns = ["judul", "isi",];

    public function getUser(){
        switch ($this->attributes["user_id"]){
            case 1:
                return "Admin";
            case 2:
                return "penonton";
        }
    }
}
