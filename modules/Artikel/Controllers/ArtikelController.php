<?php

namespace Modules\Artikel\Controllers;

use App\Models\User;
use App\Notifications\ArtikelNotifcation;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Notification;
use Modules\Artikel\Requests\Store;
use Modules\Artikel\Requests\Update;
use Modules\Artikel\Models\Artikel;
use Modules\Artikel\Tables\ArtikelTableView;

class ArtikelController extends Controller
{
    public function index()
    {
        return ArtikelTableView::make()->view('artikel::index');
    }

    public function create()
    {
        return view('artikel::create');
    }

    public function store(Store $request)
    {
        Artikel::create($request->validated());
        $artikel = new Artikel();
        $artikel->judul = $request->judul;
        $user = User::all();
        Notification::send($user, new ArtikelNotifcation('Artikel "' .$request->judul.'" telah ditambahkan'));
        return redirect()->back()->withSuccess('Artikel baru tersimpan');
    }

    public function show(Artikel $artikel)
    {
        return view('artikel::show', compact('artikel'));
    }

    public function edit(Artikel $artikel)
    {
        return view('artikel::edit', compact('artikel'));
    }

    public function update(Update $request, Artikel $artikel)
    {
        $artikel->update($request->validated());
        $user = User::all();
        Notification::send($user, new ArtikelNotifcation('Artikel "' .$artikel->judul.'" telah diedit'));
        return redirect()->back()->withSuccess('Artikel saved');
    }

    public function destroy(Artikel $artikel)
    {
        $artikel->delete();
        $user = User::all();
        Notification::send($user, new ArtikelNotifcation('Artikel "' .$artikel->judul.'" telah dihapus'));
        return redirect()->back()->withSuccess('Artikel deleted');
    }
    public function notification(){
        $user = User::find(1);
        return view('artikel::notification', compact('user'));
    }
}
