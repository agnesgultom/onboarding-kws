@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Notifikasi"/>
    <x-panel title="Notifikasi Artikel">
        <table class="ui table definition">
            @forelse($user->notifications as $notification)
                <tr>
                    <td>{{ $notification->data['message'] }}</td>
                </tr>
            @empty
                <tr><td>Belum ada data perubahan</td></tr>
            @endforelse
        </table>
    </x-panel>
@stop
