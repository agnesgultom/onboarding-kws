@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::artikel.index') }}"></x-backlink>

    <x-panel title="Tambah Artikel">
        {!! form()->post(route('modules::artikel.store'))->horizontal()->multipart() !!}
        @include('artikel::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
