@extends('laravolt::layouts.app')

@section('content')


    <x-titlebar title="Artikel">
        <x-item>
            <x-link label="Tambah" icon="plus" url="{{ route('modules::artikel.create') }}"></x-link>
            <x-link label="Notifikasi" url="{{ route('modules::artikel.notification') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! $table !!}
@stop
