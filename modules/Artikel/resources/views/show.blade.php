@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::artikel.index') }}"></x-backlink>

    <x-panel title="Detil Artikel">
        <table class="ui table definition">
        <tr><td>Id</td><td>{{ $artikel->id }}</td></tr>
        <tr><td>Judul</td><td>{{ $artikel->judul }}</td></tr>
        <tr><td>Isi</td><td>{{ $artikel->isi }}</td></tr>
        <tr><td>User Id</td><td>{{ $artikel->user_id }}</td></tr>
        <tr><td>Created At</td><td>{{ $artikel->created_at }}</td></tr>
        <tr><td>Updated At</td><td>{{ $artikel->updated_at }}</td></tr>
        </table>
    </x-panel>

@stop
