@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::artikel.index') }}"></x-backlink>

    <x-panel title="Edit Artikel">
        {!! form()->bind($artikel)->put(route('modules::artikel.update', $artikel->getKey()))->horizontal()->multipart() !!}
        @include('artikel::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
