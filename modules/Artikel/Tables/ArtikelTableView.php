<?php

namespace Modules\Artikel\Tables;

use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\Artikel\Models\Artikel;

class ArtikelTableView extends TableView
{
    public function source()
    {
        return Artikel::autoSort()->latest()->autoSearch(request('search'))->paginate();
    }

    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('judul')->sortable(),
            Text::make('isi')->sortable(),
            Text::make('user_id')->sortable(),
            RestfulButton::make('modules::artikel'),
        ];
    }
}
