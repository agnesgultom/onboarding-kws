<?php

use Modules\Artikel\Controllers\ArtikelController;

Route::group(
    [
        'prefix' => config('modules.artikel.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.artikel.route.middleware'),
    ],
    function () {
        Route::resource('artikel', ArtikelController::class);
        Route::get('artikel.notification',[ArtikelController::class, 'notification'])->name('artikel.notification')->middleware('auth');
    }
);
