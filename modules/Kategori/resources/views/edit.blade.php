@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::kategori.index') }}"></x-backlink>

    <x-panel title="Edit Kategori">
        {!! form()->bind($kategori)->put(route('modules::kategori.update', $kategori->getKey()))->horizontal()->multipart() !!}
        @include('kategori::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
