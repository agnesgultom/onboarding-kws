@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::kategori.index') }}"></x-backlink>

    <x-panel title="Detil Kategori">
        <table class="ui table definition">
        <tr><td>Id</td><td>{{ $kategori->id }}</td></tr>
        <tr><td>Nama</td><td>{{ $kategori->nama }}</td></tr>
        <tr><td>Created At</td><td>{{ $kategori->created_at }}</td></tr>
        <tr><td>Updated At</td><td>{{ $kategori->updated_at }}</td></tr>
        </table>
    </x-panel>

@stop
