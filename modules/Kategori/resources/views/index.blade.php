@extends('laravolt::layouts.app')

@section('content')


    <x-titlebar title="Kategori">
        <x-item>
            <x-link label="Tambah" icon="plus" url="{{ route('modules::kategori.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! $table !!}
@stop
