@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::kategori.index') }}"></x-backlink>

    <x-panel title="Tambah Kategori">
        {!! form()->post(route('modules::kategori.store'))->horizontal()->multipart() !!}
        @include('kategori::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
