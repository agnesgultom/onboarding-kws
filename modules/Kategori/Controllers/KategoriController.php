<?php

namespace Modules\Kategori\Controllers;

use Illuminate\Routing\Controller;
use Modules\Kategori\Requests\Store;
use Modules\Kategori\Requests\Update;
use Modules\Kategori\Models\Kategori;
use Modules\Kategori\Tables\KategoriTableView;

class KategoriController extends Controller
{
    public function index()
    {
        return KategoriTableView::make()->view('kategori::index');
    }

    public function create()
    {
        return view('kategori::create');
    }

    public function store(Store $request)
    {
        Kategori::create($request->validated());

        return redirect()->back()->withSuccess('Kategori saved');
    }

    public function show(Kategori $kategori)
    {
        return view('kategori::show', compact('kategori'));
    }

    public function edit(Kategori $kategori)
    {
        return view('kategori::edit', compact('kategori'));
    }

    public function update(Update $request, Kategori $kategori)
    {
        $kategori->update($request->validated());

        return redirect()->back()->withSuccess('Kategori saved');
    }

    public function destroy(Kategori $kategori)
    {
        $kategori->delete();

        return redirect()->back()->withSuccess('Kategori deleted');
    }
}
