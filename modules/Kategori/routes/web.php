<?php

use Modules\Kategori\Controllers\KategoriController;

Route::group(
    [
        'prefix' => config('modules.kategori.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.kategori.route.middleware'),
    ],
    function () {
        Route::resource('kategori', KategoriController::class);
    }
);
