<?php

namespace Modules\Kategori\Tables;

use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\Kategori\Models\Kategori;

class KategoriTableView extends TableView
{
    public function source()
    {
        return Kategori::autoSort()->latest()->autoSearch(request('search'))->paginate();
    }

    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('nama')->sortable(),
            RestfulButton::make('modules::kategori'),
        ];
    }
}
