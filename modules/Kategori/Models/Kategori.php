<?php

namespace Modules\Kategori\Models;

use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;

class Kategori extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'kategori';

    protected $guarded = [];

    protected $searchableColumns = ["nama",];
}
